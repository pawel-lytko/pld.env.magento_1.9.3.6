#!/bin/bash

. ./common.env
. ./common.sh
. ./.env

PATH_DUMP=$DIR_ROOT_PROJECT/core/db/shared/$FILE_NAME_DUMP_DB
PATH_USER_SQL=$DIR_ROOT_PROJECT/core/db/user.sql

if [[ $ENABLED_MYSQL_CREATE_USERS -eq 1 ]]; then
    echo "[INFO] Updating users in the database."
    $DOCKER_CMD exec -i $DB_CONTAINER_NAME mysql --user=$MYSQL_USER_DEFAULT --password=$MYSQL_ROOT_PASSWORD_DEFAULT < $PATH_USER_SQL
else
    echo "[WARNING] Updating users in the database has been disabled."
fi

if [[ -f "$PATH_DUMP" ]]; then
    echo "[INFO] Database restoration."
    $DOCKER_CMD exec -i $DB_CONTAINER_NAME mysql --user=$MYSQL_USER_DEFAULT --password=$MYSQL_ROOT_PASSWORD_DEFAULT < $PATH_DUMP
else
    echo "[WARNING] File $PATH_DUMP does not exist."
fi
