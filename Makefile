include ./make.env
include ./common.env
include ./.env
include ./user.env

DIR_ROOT_PROJECT=$(shell pwd)
DOCKER_COMPOSE_VERSION_CURRENT=$(shell docker-compose version --short)
DOCKER_COMPOSE_VERSION_VALID=$(shell expr $(DOCKER_COMPOSE_VERSION_CURRENT) \>= $(DOCKER_COMPOSE_VERSION_REQUIRED))

DOCKER_VERSION_CURRENT=$(shell docker version --format '{{.Client.Version}}')
DOCKER_VERSION_VAILD=$(shell expr $(DOCKER_VERSION_CURRENT) \>= $(DOCKER_VERSION_REQUIRED))

CURRENT_USER_ID=$(shell id -u $(USER_OWNER_FILES))
CURRENT_GROUP_ID=$(shell id -g $(GROUP_OWNER_FILES))

install: check-port check-docker-composer check-docker check-up-container
	@echo "[INFO] Start install."
	@./install_project.sh $(USER_OWNER_FILES) $(GROUP_OWNER_FILES)
	docker-compose --env-file $(DIR_ROOT_PROJECT)$(DOCKER_ENV_LOCATION) -f $(DIR_ROOT_PROJECT)$(DOCKER_COMPOSE_LOCATION) build --build-arg uid="$(CURRENT_USER_ID)" --build-arg gid="$(CURRENT_GROUP_ID)"
	@echo "End install."
	
up:
	@docker-compose --env-file $(DIR_ROOT_PROJECT)$(DOCKER_ENV_LOCATION) -f $(DIR_ROOT_PROJECT)$(DOCKER_COMPOSE_LOCATION) up -d

completed:
	@echo "[INFO] Start: Installation completed."
	@./completed.sh
	@echo "[INFO] End: Installation completed."

check-port:
	$(info [INFO] Check port: $(BIND_PORT).)

	@for PORT in $(BIND_PORT) ; do \
		PORT_PID=$$(fuser $$PORT/tcp); \
		if [ ! -z "$$PORT_PID" ]; then \
			echo "[INFO] Port $$PORT in use - please stop process PID $$PORT_PID:"; \
			lsof -i:$$PORT -P -n | grep LISTEN; \
		else \
			echo "[OK] Port $$PORT available"; \
		fi;  \
	done; \
	
	@for PORT in $(BIND_PORT) ; do \
		PORT_PID=$$(fuser $$PORT/tcp); \
		if [ ! -z "$$PORT_PID" ]; then \
			echo "[ERROR] Please stop processes that use ports."; \
			exit 1; \
		fi;  \
	done;

check-docker-composer:
	$(info [INFO] Check docker composer.)

ifneq ($(DOCKER_COMPOSE_VERSION_VALID),1)
	$(error [ERROR] Docker Compose not exist or version $(DOCKER_COMPOSE_VERSION_CURRENT) is not valid (min. $(DOCKER_COMPOSE_VERSION_REQUIRED)))
else
	$(info [OK] Docker Compose version $(DOCKER_COMPOSE_VERSION_CURRENT) vaild)
endif

check-docker:
	$(info [INFO] Check docker.)

ifneq ($(DOCKER_VERSION_VAILD),1)
	$(error [ERROR] Docker not exist or version $(DOCKER_VERSION_CURRENT) is not valid (min. $(DOCKER_VERSION_REQUIRED)))
else
	$(info [OK] Docker version $(DOCKER_VERSION_CURRENT) vaild)
endif

check-up-container:
	$(info [INFO] Checking if the containers are running: $(LIST_CONTAINER).)

	@for CONTAINER in $(LIST_CONTAINER) ; do \
		NAME_CONTAINER=$$(docker ps --format '{{.Names}}' | grep -w $$CONTAINER); \
		if [ ! -z "$$NAME_CONTAINER" ]; then \
			echo "[WARNING] Running container: $$CONTAINER."; \
		else \
			echo "[OK] Not running container: $$CONTAINER."; \
		fi; \
	done; \
	
	@for CONTAINER in $(LIST_CONTAINER) ; do \
		NAME_CONTAINER=$$(docker ps --format '{{.Names}}' | grep -w $$CONTAINER); \
		if [ ! -z "$$NAME_CONTAINER" ]; then \
			echo "[ERROR] Stop containers, commands use: docker-composer stop and make reinstall."; \
			exit 1; \
		fi; \
	done;

check-exists-container:
	$(info [INFO] Check exists containers: $(LIST_CONTAINER).)

	@for CONTAINER in $(LIST_CONTAINER) ; do \
		NAME_CONTAINER=$$(docker ps -a --format '{{.Names}}' | grep -w $$CONTAINER); \
		if [ ! -z "$$NAME_CONTAINER" ]; then \
			echo "[OK] The container exists: $$CONTAINER."; \
		else \
			echo "[WARNING] The container does not exist: $$CONTAINER."; \
		fi; \
	done; \

	@for CONTAINER in $(LIST_CONTAINER) ; do \
		NAME_CONTAINER=$$(docker ps -a --format '{{.Names}}' | grep -w $$CONTAINER); \
		if [ -z "$$NAME_CONTAINER" ]; then \
			echo "[ERROR] Command use: make install."; \
			exit 1; \
		fi; \
	done;
