#!/bin/bash

. ./common.env
. ./common.sh

CONTAINER_FLAG=

while [ $# -gt 0 ]; do
	case "$1" in
		--container*|-c*)
			if [[ "$1" != *=* ]]; then shift; fi
			CONTAINER_FLAG="${1#*=}"
			;;
		--help|-h)
			printf "#####################################################################################################################\n"
			printf "###### Parameters\n"
			printf "       --container, -c   Container flag: www => container entry to: www_mt, redis => container entry to: redis_mt, mysql => container entry to: mysql_mt.\n"
			printf "       --help, -h        This screen.\n"
			printf "#####################################################################################################################\n"
			exit 0
			;;
		*)
			printf "#####################################################################################################################\n"
			printf "###### Parameters\n"
			printf "       --container, -c   Container flag: www => container entry to: www_mt, redis => container entry to: redis_mt, mysql => container entry to: mysql_mt.\n"
			printf "       --help, -h        This screen.\n"
			printf "#####################################################################################################################\n"
			exit 0
			;;
	esac

	shift
done

case "$CONTAINER_FLAG" in
    www)
    $DOCKER_CMD exec -u $USER_BASIC_CONTAINER_WWW -it $WWW_CONTAINER_NAME bash
    ;;
    redis)
    $DOCKER_CMD exec -it $REDIS_CONTAINER_NAME bash
    ;;
    mysql)
    $DOCKER_CMD exec -it $DB_CONTAINER_NAME bash
    ;;
esac
