Instalacja kontenerów
================================


Etykieta | Wartość
-------- | --------
Wersja | 0.0.0.1
Autor | Paweł Łytko
E-mail autor | pawel.lytko@gmail.com
Data utworzenia | 3 września 2021
Data modyfikacji | 3 września 2021

## Historia zmian

Data zmiany | autor i e-mail autora | Opis zmiany
----------- | -------- | ------------
3 września 2021 | Paweł Łytko <pawel.lytko@gmail.com> | Utworzenie dokumentacji

## Wymagania

- Linux
- Docker >= 20.10
- Docker compose >= 1.27
- Make >= 4.2
- sudo

## Do pobrania

- Magento dotyczy wcześniejszych wersji: 1.*: [link]
(https://blog.magestore.com/magento-download/#magento1)

## Konfiguracja

Utworzyć i uzupełnić poniższe pliki:

- Plik: `common.sh`
- Plik: `.env`
- Plik: `user.env`

Można utworzyć nowy plik lub skopiować pliki w katalogu projektu:

`cp common.sh.example common.sh`

`cp .env.example .env`

`cp user.env.example user.env`

Każdy plik posiada swój odpowiednik w plikach: `*.example`, który posiada kompletną strukturę z przykładowymi wartościami.

### Opis pliku konfiguracyjne: common.sh

Nazwa | Wartość domyślna | Opis
-------- | -------- | --------
DOCKER_CMD | 'sudo docker' | polecenie użyte w skryptach
DOCKER_COMPOSE_CMD | 'sudo docker-compose' | polecenie użyte w skryptach
FILE_NAME_DUMP_DB | 'dump.sql' | Nazwa pliku z dump'em bazy danych. W bieżącej wersji nie używany.
ENABLED_MYSQL_CREATE_USERS | 1 | Parametr włącza / wyłącza przygotowanie użytkowników dla bazy danych. Wymagane wartości 1 => włącza, 0 => wyłącza.

### Opis pliku konfiguracyjne: .env

Nazwa | Wartość domyślna | Opis
-------- | -------- | --------
COMPOSE_PROJECT_NAME | eemt | Nazwa projektu, **nie zmieniać**.
REDIS_PASSWORD_DEFAULT | root | Domyślne hasło serwera Redis. 
MYSQL_ROOT_PASSWORD_DEFAULT | root | Domyślne hasło domyślnego użytkownika. Należy skorzystać z wartości domyślnych. W przypadku zmiany wartość trzeba zmodyfikować plik: `core/db/user.sql`. Nie należy wypychać zmian do repozytorium kodu.
MYSQL_USER_DEFAULT | root | Domyślny użytkownik serwera MySQL, **nie zmieniać**.
DIR_PROJECT | | Katalog projektu kodu, który jest utworzony podczas wykonania polecenia `make install`.
SUBNET_IP | | podsieć w formacie CIDR reprezentująca segment sieci.
REDIS_IPV4_ADDRESS | | adres IP kontenera `redis_`.
MYSQL_IPV4_ADDRESS | | adres IP kontenera `mysql_`.


### Opis pliku konfiguracyjne: user.env

Nazwa | Wartość domyślna | Opis
-------- | -------- | --------
USER_OWNER_FILES | | Nazwa użytkownika dla właściciela plików / katalogów projektu kodu
GROUP_OWNER_FILES | | Nazwa grupy dla właściciela plików / katalogów projektu kodu

## Kolejność czynności

- Przygotowanie plików konfiguracyjnych
- Instalacja lub przebudowanie
- Finalizacja instalacji

## Instalacja

`make install`

## Uruchomienie kontenery

`make up`

## Finalizacja instalacji

`make completed`

## Kolejność poleceń przy instalacji

1. Uzupełnienie konfiguracji dla plików:

- `common.sh`
- `.env`
- `user.env`

Zobacz punkt: **Konfiguracja**

2. Ustawienie rekordu w pliku: `/etc/hosts`. Dodać adres IP zgodny z maską podsieci, np.: `172.25.0.1` dla maski: `172.25.0.0/16`. Nazwa hosta znajduje się w pliku: `pim-dev.bch.localhost.conf`. Domyślna nazwa hosta: `pim-dev.bch.localhost`. Dostęp do bazy danych należy skonfigurowany zgodnie z powyższymi wskazówkami jak w przykładzie, np. przez adres IP: `172.25.0.1`.
3. Polecenie: `make install`.
4. Polecenie: `make up`.
5. Polecenie: `make completed`.
6. Zatrzymanie pracy kontenerów, przez polecenie `stop.sh`.
7. Instalacja kodu projektu w katalogu `src/{DIR_PROJECT}`.
8. Konfiguracja projektu.

## Objaśnienie poleceń instalacji kontenerów

**Polecenie: `make install`**:

1. Instalacja katalogu na podstawie wartości z parametru: `DIR_PROJECT` o masce: `777`. Właściciel katalogu jest ustawiony z parametrów: `USER_OWNER_FILES` i `GROUP_OWNER_FILES`.
2. Budowa kontenerów. W celu zachowaniu ustawienia uprawnień do plików / katalogów projektu ustawione są właścicielami z parametrów: `USER_OWNER_FILES` i `GROUP_OWNER_FILES`. Dotyczy to konteneru: `www-mt`. W wymienionych kontenerów znajduje się katalog: `/var/www`, który wskazuje katalog na komputerze host'a: `usr/{DIR_PROJECT}`

**Polecenie: `make up`**:

Uruchomienie zbudowanych kontenerów.

**Polecenie: `make completed`**:

1. Przygotowanie użytkowników bazy danych, patrz plik: `core/db/user.sql`.
2. Przywrócenie bazy danych na podstawie pliku umieszczonego w katalogu: `core/db/shared/`. Parametr: `FILE_NAME_DUMP_DB` ustawia plik z dump'em bazy danych. Do przywrócenia bazy danych jest wykorzystane polecenie `mysql`. Jeśli nie będzie pliku w katalogu: `core/db/user.sql` wówczas nie odbędzie się przywrócenie bazy danych.

## Przydatne polecenia

- `ifconfig`
    - podręcznik: [link]
(https://man7.org/linux/man-pages/man8/ifconfig.8.html)
    - instalacja w Ubuntu: `sudo apt install net-tools`
- `sudo`
    - podręcznik: [link]
(http://manpages.ubuntu.com/manpages/xenial/en/man8/sudo.8.html)
    - instalacja w Ubuntu: `apt-get install sudo`
- `id`
    - podręcznik: [link]
(https://man7.org/linux/man-pages/man1/id.1.html)
- `docker`
    - podręcznika: [link]
(https://docs.docker.com/engine/reference/commandline/cli/)
    - instalacja: [link]
(https://docs.docker.com/engine/install/)
- `docker-compose`
    - podręcznik: [link]
(https://docs.docker.com/compose/reference/)
    - instalacja: [link]
(https://docs.docker.com/compose/install/)

- `up.sh`

Polecenie uruchamia kontenery.

Plik znajduje się w katalogu: `bch-pimcore-docker`.

- `stop.sh`

Polecenie zatrzymujące uruchomione kontenery.

Plik znajduje się w katalogu: `bch-pimcore-docker`.

- `console.sh`

Polecenie uruchamia kontenery bash. Polecenie posiada dwa parametry:

Parametr | Opis 
-------- | --------
--container, -c | Flagi reprezentujące kontenery. Flaga: `www` to `www-mt`
--help, -h | Ekran pomocy z opisem polecenia

Przykład:

`console.sh -c www`

`console.sh --container=www`

`console.sh --help`

## Lista serwisów w docker-compose.yml

## FQA

#### Klonowanie repozytorium w istniejącym katalogu

Dodanie znaku kropki za `<repository-url>`

`git clone <repository-url> .`

Polecenie wydać w katalogu, w którym ma być repozytorium.

Kolejnym sposobem jest wydanie poleceń:

`git init .`

`git remote add origin <repository-url>`

`git pull origin master`

## Struktura katalogu projektu
```
src
	.gitignore
bch-pimcore-docker
    core
        data
                mysql
                		.gitignore
                redis
                		.gitignore
        db
                shared
                		.gitignore
                Dockerfile
                my.cnf
                user.sql
        redis
                Dockerfile
                redis.conf
    common.env
    common.sh
    common.sh.example
    completed.sh
    console.sh
    docker-compose.yml
    install_project.sh
    .env
    .env.example
    make.env
    Makefile
    README.md
    stop.sh
    up.sh
    user.env
    user.env.example
```

### Opis struktury katalogu projektu

Nazwa | Rodzaj | Opis
----- | ------ | ------
`src` | katalog | katalog przeznaczony dla kodu projektu
`.gitignore` | plik | plik git'a wykluczające wszystkie pliki, dodany w celu wykluczenia katalogu projektu
`core` | katalog | zawiera pliki potrzebny do utworzenia i obsługą kontenerów
`data` | katalog | przeznaczone do przechowywania plików danych usług takich jak serwer MySQL, Redis
`mysql` | katalog | przeznaczony dla plików baz danych serwera MySQL
`.gitignore` | plik | plik git'a wykluczające wszystkie pliki
`redis` | plik | przeznaczony dla plików serwera Redis
`.gitignore` | plik | plik git'a wykluczające wszystkie pliki
`db` | katalog | pliki z definicją kontenera bazy danych
`shared` | katalog | udostępniony w kontenerze bazy danych
`.gitignore` | plik | plik git'a wykluczające wszystkie pliki
`Dockerfile` | plik | definicja kontenera bazy danych
`my.cnf` | plik | konfiguracja serwera MySQL
`user.sql` | plik | skrypt serwisowania użytkowników baz danych
`redis` | katalog | przeznaczony dla plików baz danych serwera Redis
`Dockerfile` | plik | definicja kontenera Redis
`redis.conf` | plik | konfiguracja serwera Redis
`common.env` | plik | zawiera wspólne ustawienia dla plików `*.sh` i pliku `Makefile`
`common.sh` | plik | zawiera wspólne ustawienia używany tylko w plikach `*.sh`
`common.sh.example` | plik | przykładowy pliku `common.sh`
`completed.sh` | plik | skrypt uruchamiany na zakończenie instalacji
`console.sh` | plik | polecenie, które uruchamia polecenie `bash` w kontenerze `www_mt`
`docker-compose.yml` | plik | konfiguracyjny docker'a
`install_project.sh` | plik | polecenia wykonywane podczas instalacji projektu kodu, który są wykonane przed zbudowaniem kontenerów
`.env` | plik | plik z ustawieniami kontenerów
`.env.example` | plik | przykładowy plik `.env`
`make.env` | plik | ustawienia dla pliku `Makefile`
`Makefile` | plik | skrypt do instalacji / reinstalcji i finalizacja instalacja
`README.md` | plik | dokumentacja
`stop.sh` | plik | polecenie zatrzymujące uruchomione kontenery
`up.sh` | plik | polecenie uruchamia kontenery
`user.env` | plik | ustawienia właściciela plików / katalogów projektu kodu
`user.env.example` | plik | przykładowy plik dla ustawienia właściciela plików / katalogów projektu kodu
